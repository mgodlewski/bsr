package com.bsr;

/**
 * main application class
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BsrApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(BsrApplication.class, args);
	}
	
}
