package com.bsr.models;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

public interface TransferDao extends CrudRepository<Transfer, Long>{

	public ArrayList<Transfer> findByAccountFromOrAccountTo(Account accountFrom, Account accountTo);
}
