package com.bsr.models;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

public interface ExternalTransferDao extends CrudRepository<ExternalTransfer, Long>{

	public ArrayList<ExternalTransfer> findByAccountNumberFromOrAccountNumberTo(String accountNumberFrom, String accountNumberTo);
}
