package com.bsr.models;

import org.springframework.data.repository.CrudRepository;

public interface MaxAccountNumberDao extends CrudRepository<MaxAccountNumber, Long>{

	public MaxAccountNumber findById(long id);
}
