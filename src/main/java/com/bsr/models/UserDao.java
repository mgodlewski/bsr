package com.bsr.models;

import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long>{
	
	public User findByLogin(String login);
	
	public User findById(long id);

}
