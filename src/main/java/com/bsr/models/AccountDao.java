package com.bsr.models;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;

public interface AccountDao extends CrudRepository<Account, Long>{

	public ArrayList<Account> findByUser(User user);
	
	public Account findByNumber(String number);
}
