package com.bsr.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "transfers")
public class Transfer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable=false)
	private long id;
	
	@NotNull
	private Date date;
	
	@NotNull
	private String title;
	
	@Column(nullable=true)
	private int balanceFrom;
	
	@NotNull
	private int balanceTo;
	
	@NotNull
	private int amount;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="account_id_from", nullable=true)
	private Account accountFrom;
	
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="account_id_to", nullable=true)
	private Account accountTo;

	public Transfer(){}
	
	public Transfer(String title, int balanceFrom, int balanceTo, int amount, Account accountFrom,
			Account accountTo) {
		this.date = new Date();
		this.title = title;
		this.balanceFrom = balanceFrom;
		this.balanceTo = balanceTo;
		this.amount = amount;
		this.accountFrom = accountFrom;
		this.accountTo = accountTo;
	}
		
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getBalanceFrom() {
		return balanceFrom;
	}

	public void setBalanceFrom(int balanceFrom) {
		this.balanceFrom = balanceFrom;
	}

	public int getBalanceTo() {
		return balanceTo;
	}

	public void setBalanceTo(int balanceTo) {
		this.balanceTo = balanceTo;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Account getAccountFrom() {
		return accountFrom;
	}

	public void setAccountFrom(Account accountFrom) {
		this.accountFrom = accountFrom;
	}

	public Account getAccountTo() {
		return accountTo;
	}

	public void setAccountTo(Account accountTo) {
		this.accountTo = accountTo;
	}
}
