package com.bsr.endpoint;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bsr.Utils;
import com.bsr.models.Account;
import com.bsr.models.AccountDao;
import com.bsr.models.MaxAccountNumber;
import com.bsr.models.MaxAccountNumberDao;
import com.bsr.models.User;
import com.bsr.models.UserDao;

import io.spring.guides.gs_producing_web_service.GetAccountsRequest;
import io.spring.guides.gs_producing_web_service.GetAccountsResponse;
import io.spring.guides.gs_producing_web_service.PostAccountRequest;
import io.spring.guides.gs_producing_web_service.PostAccountResponse;

@Endpoint
public class AccountEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private UserDao userDao;
	
	@Value("${bankNumber}")
	private String serverBankNumber;
	
	@Autowired
	private MaxAccountNumberDao maxAccountNumberDao;

	/**
	* @param PostAccountRequest object with user id
	* @return new account object
	*/	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "postAccountRequest")
	@ResponsePayload
	public PostAccountResponse createAccount(@RequestPayload PostAccountRequest request) {
		PostAccountResponse response = new PostAccountResponse();
		Account account = null;
		try{
			User user = userDao.findById(request.getUserId());
			account = new Account(user);
			account.setNumber(generateNumber());
			
			accountDao.save(account);
			io.spring.guides.gs_producing_web_service.Account a = new io.spring.guides.gs_producing_web_service.Account();
			a.setId(account.getId());
			a.setBalance(account.getBalance());
			a.setNumber(account.getNumber());
			response.setAccount(a);
		} catch (Exception ex){
			System.out.println("Error creating account: " + ex.toString());
			io.spring.guides.gs_producing_web_service.Account a = new io.spring.guides.gs_producing_web_service.Account();
			a.setId(-1);
			response.setAccount(a);
		}
		return response;
	}
	
	/**
	* @param GetAccountsRequest object with user id
	* @return all user accounts
	*/
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAccountsRequest")
	@ResponsePayload
	public GetAccountsResponse getAccount(@RequestPayload GetAccountsRequest request) {
		GetAccountsResponse response = new GetAccountsResponse();
		List<Account> accounts = new ArrayList<>();
		try{
			User user = userDao.findById(request.getUserId());
			accounts.addAll(accountDao.findByUser(user));
			for(int i=0; i<accounts.size(); i++){
				io.spring.guides.gs_producing_web_service.Account a = new io.spring.guides.gs_producing_web_service.Account();
				a.setId(accounts.get(i).getId());
				a.setBalance(accounts.get(i).getBalance());
				a.setNumber(accounts.get(i).getNumber());
				response.getAccount().add(a);
			}
		} catch (Exception ex){
			System.out.println("Error getting account: " + ex.toString());
		}
		return response;
	}
	
	private String generateNumber() {
		String n = serverBankNumber;
		try{
			String maxStr = maxAccountNumberDao.findById(1).getNumber();
			if(maxStr==null){
				maxAccountNumberDao.save(new MaxAccountNumber());
				return Utils.calculateChecksum(n+"0000000000000000");
			}
			
			Long maxL = Long.parseLong(maxStr);
			maxL++;
			maxStr = String.valueOf(maxL);
			
			while(maxStr.length()<16){
				maxStr = "0" + maxStr;
			}
			
			MaxAccountNumber man = new MaxAccountNumber();
			man.setId(1);
			man.setNumber(maxStr);
			maxAccountNumberDao.save(man);
			return Utils.calculateChecksum(n+maxStr);
		}catch(Exception e){
			System.out.println("Can not save maxAccountNumber" + e);
			return "0";
		}
	}
	
}
