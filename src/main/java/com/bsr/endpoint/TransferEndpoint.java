package com.bsr.endpoint;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;

import javax.transaction.Transactional;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bsr.Utils;
import com.bsr.models.Account;
import com.bsr.models.AccountDao;
import com.bsr.models.ExternalTransfer;
import com.bsr.models.ExternalTransferDao;
import com.bsr.models.Transfer;
import com.bsr.models.TransferDao;
import com.bsr.rest.TransferModel;

import io.spring.guides.gs_producing_web_service.GetTransfersRequest;
import io.spring.guides.gs_producing_web_service.GetTransfersResponse;
import io.spring.guides.gs_producing_web_service.PostTransferRequest;
import io.spring.guides.gs_producing_web_service.PostTransferResponse;

@Endpoint
@Transactional
public class TransferEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	
	@Value("${bankNumber}")
	private String serverBankNumber;
	
	@Value("${index_ip.csv}")
	private String indexIp;
	
	@Value("${basicAuthenticationUser}")
	private String authUser;
	
	@Value("${basicAuthenticationPassword}")
	private String authPass;
	 
	@Autowired
	private TransferDao transferDao;
	
	@Autowired
	private ExternalTransferDao externalTransferDao;
	
	@Autowired
	private AccountDao accountDao;

	/**
	* @param PostTransferRequest
	* @return code: 1 - transfer is ok, -2 - forbidden, -3 - notFound, -4 - badRequest, -5 - not enough money, -6 - not enough data, -7 -unauthorized
	*/	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "postTransferRequest")
	@ResponsePayload
	public PostTransferResponse makeTransfer(@RequestPayload PostTransferRequest request) {
		PostTransferResponse response = new PostTransferResponse();
		try{
			int amount = -1;
			try{
				amount = request.getTransfer().getAmount();
			}catch(Exception e){
				response.setCode(-4);
				return response;
			}
			
			String from = request.getTransfer().getFrom();
			String to = request.getTransfer().getTo();
			String title = request.getTransfer().getTitle();
			
			if (to.length()>0 || title.length()>0){
				if(Utils.checkChecksum(to)){
					Account accountFrom = accountDao.findByNumber(from);
					if(amount>0){
						if(accountFrom.getBalance()>=amount){
							String bankNumber = to.substring(2,10);
							if(bankNumber.equals(serverBankNumber)){
								Account accountTo = accountDao.findByNumber(to);
								if(accountTo!=null){
									accountFrom.setBalance(accountFrom.getBalance()-amount);
									accountTo.setBalance(accountTo.getBalance()+amount);
									accountDao.save(accountFrom);
									accountDao.save(accountTo);
									int balanceFrom = accountFrom.getBalance();
									int balanceTo = accountTo.getBalance();
									Transfer transfer = new Transfer(title, balanceFrom, balanceTo, amount, accountFrom, accountTo);
									transferDao.save(transfer);
									response.setCode(1);
								}else{	//konto nie istnieje
									response.setCode(-3);
								}
							}else{	//przekazanie transferu do innego banku
								String url = getIpFromIndex(bankNumber);
								if(url.equals("")){	//konto nie istnieje
									response.setCode(-3);
								}else{
									HttpStatus httpStatus = sendTransferToOtherBank(to, title, amount, from, url);
									switch (httpStatus){
						        	case CREATED:
						        		accountFrom.setBalance(accountFrom.getBalance()-amount);
						        		int balanceFrom = accountFrom.getBalance();
										accountDao.save(accountFrom);
										
										ExternalTransfer transfer = new ExternalTransfer(title, amount, accountFrom.getNumber(), to, balanceFrom);
										externalTransferDao.save(transfer);
										
						        		response.setCode(1);
						        		break;
						        	case NOT_FOUND:
						        		response.setCode(-3);
						        		break;
						        	case BAD_REQUEST:
						        		response.setCode(-4);
						        		break;
						        	case FORBIDDEN:
						        		response.setCode(-2);
						        		break;
						        	case UNAUTHORIZED:
						        		response.setCode(-7);
						        		break;
									default:
										response.setCode(-1);
										break;
									}
								}
							}
						}else{	//nie wystarczająca ilość pieniędzy na koncie
							response.setCode(-5);
						}
					}else{	//podano ujemną lub zerową kwotę
						response.setCode(-4);
					}
				}else{	//błąd sumy kontrolnej numeru docelowego
					response.setCode(-2);
				}
			}else{	//nie podano wymaganych danych
				response.setCode(-6);
			}
		} catch (Exception ex){
			System.out.println("Error making transfer: " + ex);
			ex.printStackTrace();
			response.setCode(-1);
		}
		return response;
	}

	/**
	* @param GetTransfersRequest with accountNumber
	* @return history for that account
	*/	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTransfersRequest")
	@ResponsePayload
	public GetTransfersResponse getTransfers(@RequestPayload GetTransfersRequest request) {
		GetTransfersResponse response = new GetTransfersResponse();
		try{
			String number = request.getAccountNumber();
			Account account = accountDao.findByNumber(number);
			
			ArrayList<io.spring.guides.gs_producing_web_service.TransferGet> transfersGet = new ArrayList<>();
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			
			ArrayList<Transfer> transfers = transferDao.findByAccountFromOrAccountTo(account, account);			
			for(Transfer transfer : transfers){	
				io.spring.guides.gs_producing_web_service.TransferGet tr = new io.spring.guides.gs_producing_web_service.TransferGet();
				tr.setAmount(transfer.getAmount());
				tr.setBalanceFrom(transfer.getBalanceFrom());
				tr.setBalanceTo(transfer.getBalanceTo());
				tr.setDate(df.format(transfer.getDate()));
				if(transfer.getAccountFrom()!=null){
					tr.setFrom(transfer.getAccountFrom().getNumber());
				}else{
					tr.setFrom("");
				}
				tr.setTo(transfer.getAccountTo().getNumber());
				tr.setTitle(transfer.getTitle());
				transfersGet.add(tr);
				response.getTransfer().add(tr);
			}
			
			ArrayList<ExternalTransfer> externaltransfers = externalTransferDao.findByAccountNumberFromOrAccountNumberTo(account.getNumber(), account.getNumber());
			for (ExternalTransfer externalTransfer : externaltransfers){
				io.spring.guides.gs_producing_web_service.TransferGet tr = new io.spring.guides.gs_producing_web_service.TransferGet();
				tr.setAmount(externalTransfer.getAmount());
				tr.setBalanceFrom(externalTransfer.getBalance());
				tr.setBalanceTo(externalTransfer.getBalance());
				tr.setDate(df.format(externalTransfer.getDate()));
				tr.setFrom(externalTransfer.getAccountNumberFrom());

				tr.setTo(externalTransfer.getAccountNumberTo());
				if(externalTransfer.getTitle().contains("/b")){
					tr.setTitle("");
				}else{
					tr.setTitle(externalTransfer.getTitle());
				}
				transfersGet.add(tr);
				response.getTransfer().add(tr);
			}
		
		} catch (Exception ex){
			System.out.println("Error geting transfers: " + ex);
			ex.printStackTrace();
		}
		return response;
	}
	
	private HttpStatus  sendTransferToOtherBank(String toAccount, String title, int amount, String fromAccount, String url){
		url = "http://" + url + "/accounts/" + toAccount;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		
		try {
			String plainCreds = authUser + ":" + authPass;
			byte[] plainCredsBytes  = plainCreds.getBytes("UTF-8");
			String base64Creds = new String(Base64.getEncoder().encode(plainCredsBytes));
			headers.add("Authorization", "Basic " + base64Creds);
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		
		JSONObject req = new JSONObject();
		try {
			req.put("amount", amount);
			req.put("from", fromAccount);
			req.put("title", title);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		HttpEntity<String> request = new HttpEntity<String>(req.toString(), headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<TransferModel> response = restTemplate.postForEntity(url, request , TransferModel.class);
		return response.getStatusCode();
	}
	
	private String getIpFromIndex(String index){
		String ip = "";
		String line = "";
		try(BufferedReader br = new BufferedReader(new FileReader(indexIp))){
			while ((line = br.readLine()) != null){
				String[] bank = line.split(",");
				if(bank[0].equals(index)){
					ip = bank[1];
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ip;
	}
}
