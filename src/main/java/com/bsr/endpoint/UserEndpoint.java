package com.bsr.endpoint;

import java.security.MessageDigest;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bsr.models.User;
import com.bsr.models.UserDao;

import io.spring.guides.gs_producing_web_service.LoginRequest;
import io.spring.guides.gs_producing_web_service.LoginResponse;
import io.spring.guides.gs_producing_web_service.PostUserRequest;
import io.spring.guides.gs_producing_web_service.PostUserResponse;
import io.spring.guides.gs_producing_web_service.UserRequest;
import io.spring.guides.gs_producing_web_service.UserResponse;

@Endpoint
public class UserEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
	
	@Autowired
	private UserDao userDao;

	/**
	* @param PostUserRequest with user data
	* @return new userID, or -1 if can not create user
	*/		
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "postUserRequest")
	@ResponsePayload
	public PostUserResponse createUser(@RequestPayload PostUserRequest request) {
		PostUserResponse response = new PostUserResponse();
		try{
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			byte[] passwordBytes = request.getUser().getPassword().getBytes("UTF-8");
	        byte[] md5Password = md5.digest(passwordBytes);
	        byte[] md5PasswordB64 = Base64.getEncoder().encode(md5Password);
	        String passStrEncode = new String(md5PasswordB64);
	        
			User user = new User(request.getUser().getName(),request.getUser().getSurname(),
					passStrEncode,request.getUser().getLogin());
			userDao.save(user);
			response.setId(user.getId());
		} catch (Exception ex){
			System.out.println("Error creating user: " + ex.toString());
			response.setId(-1);
		}
		return response;
	}

	/**
	* @param LoginRequest with login and password
	* @return -1 - can not log in, 10 user if logged on
	*/	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "loginRequest")
	@ResponsePayload
	public LoginResponse login(@RequestPayload LoginRequest request) {
		LoginResponse response = new LoginResponse();
		User user = null;
		try{
			user = userDao.findByLogin(request.getLogin());
			if(user != null){
				
				byte[] passwordBytes = user.getPassword().getBytes("UTF-8");
		        MessageDigest md = MessageDigest.getInstance("SHA-1");
		        byte[] digestedPassword = md.digest(passwordBytes);
		        byte[] passwordB64 = Base64.getEncoder().encode(digestedPassword);
		        String passStrEncode = new String(passwordB64);
		        
				if(passStrEncode.equals(request.getPassword())){
					System.out.println("User: " + user.getLogin() + " login succesfull!");
					response.setSessionId(10);
				}else{
					response.setSessionId(-1);
				}
			}else{
				response.setSessionId(-1);
			}			
		} catch (Exception ex){
			System.out.println("Error login user: " + ex.toString());
			response.setSessionId(-1);
		}
		return response;
	}
	
	
	/**
	* @param UserRequest with user login
	* @return return user object
	*/	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "userRequest")
	@ResponsePayload
	public UserResponse getUser(@RequestPayload UserRequest request) {
		UserResponse response = new UserResponse();
		User user = null;
		try{
			user = userDao.findByLogin(request.getLogin());
			response.setLogin(user.getLogin());
			response.setName(user.getName());
			response.setSurname(user.getSurname());
			response.setId(user.getId());
		} catch (Exception ex){
			System.out.println("Error login user: " + ex.toString());
		}
		return response;
	}
}
