package com.bsr.endpoint;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.bsr.Utils;
import com.bsr.models.Account;
import com.bsr.models.AccountDao;
import com.bsr.models.Transfer;
import com.bsr.models.TransferDao;

import io.spring.guides.gs_producing_web_service.MakeDepositRequest;
import io.spring.guides.gs_producing_web_service.MakeDepositResponse;

@Endpoint
@Transactional
public class DepositEndpoint {
	private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private TransferDao transferDao;
	
	
	/**
	* consume deposit request
	* @param MakeDepositRequest
	* @return code: 1-ok, -5 - not enought money, -3 - account does not exist, -2 - checksum error, -6 - not enought data
	*/
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "makeDepositRequest")
	@ResponsePayload
	public MakeDepositResponse makeDeposit(@RequestPayload MakeDepositRequest request) {
		MakeDepositResponse response = new MakeDepositResponse();
		try{
			int amount = request.getAmount();
			String to = request.getTo();
			String title = request.getTitle();
			if (to.length()>0 || title.length()>0){
				if(Utils.checkChecksum(to)){
					Account accountTo = accountDao.findByNumber(to);
					if(accountTo!=null){
						if (!(amount<0 && accountTo.getBalance()+amount<0)){
							accountTo.setBalance(accountTo.getBalance()+amount);
							accountDao.save(accountTo);
							int balanceTo = accountTo.getBalance();
							Transfer transfer = new Transfer(title, 0, balanceTo, amount, null, accountTo);
							transferDao.save(transfer);
							response.setCode(1);
				    		System.out.println("Save deposit to: " + accountTo.getNumber() + " amount: " + amount + " title: " + title);
						}else{	//nie ma wystarczającerj ilości pieniędzy na koncie
							response.setCode(-5);
						}
					}else{	//konto nie istnieje
						response.setCode(-3);
					}
				}else{	//błąd sumy kontrolnej numeru docelowego
					response.setCode(-2);
				}
			}else{	//nie podano wymaganych danych
				response.setCode(-6);
			}
		} catch (Exception ex){
			System.out.println("Error making deposit: " + ex);
			ex.printStackTrace();
			response.setCode(-1);
		}
		return response;
	}
}
