package com.bsr.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.bsr.rest.TransferModel;

import java.io.BufferedReader;
import java.io.FileReader;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Controller for testing if app is working correctly
 * @param 
 * @return 
 */
@Controller
public class MainController {

  @Value("${bankNumber}")
  private String bankNumber;
  
  @Value("${index_ip.csv}")
  private String indexIp;
	
  @RequestMapping("/")
  @ResponseBody
  public String index() {
//	sendTransfer("toAccount", "title", 100, "fromAccount");
	  
	return "ip: " + getIpFromIndex("109754");
  }
  
  public void  sendTransfer(String toAccount, String title, int amount, String fromAccount){
		String url = "http://localhost:8080/accounts/"+toAccount;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		JSONObject req = new JSONObject();
		try {
			req.put("amount", amount);
			req.put("from", fromAccount);
			req.put("title", title);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		HttpEntity<String> request = new HttpEntity<String>(req.toString(), headers);
		
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<TransferModel> response = restTemplate.postForEntity(url, request , TransferModel.class);
		System.out.println("kod odpowiedzi: " + response.getStatusCode());
  }
  
  /**
   * Map index to IP
   * @param student index
   * @return ip connected with index
   */
  private String getIpFromIndex(String index){
		String ip = "";
		String line = "";
		try(BufferedReader br = new BufferedReader(new FileReader(indexIp))){
			while ((line = br.readLine()) != null){
				String[] bank = line.split(",");
				if(bank[0].equals(index)){
					ip = bank[1];
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ip;
	}

}