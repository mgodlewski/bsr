package com.bsr;


public abstract class Utils {

	/**
	 * @param account number without checksum
	 * @return account number with calculated checksum
	 */
	public static String calculateChecksum(String number){
		number = number.replace(" ", "");
		String nr = number + "252100";
		int modulo = 0;
		for(int i=0; i<nr.length(); i++){
			modulo = (10*modulo + Character.getNumericValue(nr.charAt(i)))%97;
		}
		modulo = 98-modulo;
		if(modulo<10){
			return "0"+String.valueOf(modulo)+number;
		}else{
			return String.valueOf(modulo)+number;
		}
	}
	
	/**
	 * @param account number with checksum
	 * @return true if checksum is correct, false otherwise
	 */
	public static Boolean checkChecksum(String number){
		number = number.replace(" ", "");
		String nr = "";
		for(int i=0; i<number.length(); i++){
			nr += Character.getNumericValue(number.charAt(i));
		}
		nr += "2521" + Character.getNumericValue(nr.charAt(0)) + Character.getNumericValue(nr.charAt(1));
		nr = nr.substring(2, nr.length());
		int modulo = 0;
		for(int i=0; i<nr.length(); i++){
			modulo = (10*modulo + Character.getNumericValue(nr.charAt(i)))%97;
		}
		
		if(modulo==1){
			return true;
		}else{
			return false;
		}
	}
}
