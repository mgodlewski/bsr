package com.bsr.rest;


public class TransferModel {

	private int amount;
	private String from;
	private String title;
	
	public TransferModel(){}
	
	public TransferModel(int amount, String from, String title){
		this.amount = amount;
		this.from = from;
		this.title = title;
	}
	
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
