package com.bsr.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bsr.Utils;
import com.bsr.models.Account;
import com.bsr.models.AccountDao;
import com.bsr.models.ExternalTransfer;
import com.bsr.models.ExternalTransferDao;

@RestController
@RequestMapping("/accounts/{accountNumber}")
public class TransferController {
	
	@Autowired
	private AccountDao accountDao;
	
	@Autowired
	private ExternalTransferDao externalTransferDao;

	/**
	* consume transfer from external account
	* @param destination account number, TransferModel object containing amount, title and source account
	* @return 201 - transfer is ok, 404 - one of account does not exist, 400 - amount is negative, 500 -  server error, 403 - checksum error
	*/
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> receiveTransfer(@PathVariable String accountNumber, @RequestBody TransferModel input) {
		HttpHeaders headers = new HttpHeaders();
		
		int amount = -1;
		try{
			amount = input.getAmount();
		}catch(Exception e){
			return new ResponseEntity<>(new ErrorResponse("Negative ammount error!") ,headers, HttpStatus.BAD_REQUEST);
		}
		String from = input.getFrom();
		String title = input.getTitle();
		
		System.out.println("accountNumber " + accountNumber);
		System.out.println("amount " + amount);
		System.out.println("from " + from);
		System.out.println("title " + title);
		
		try{
			if(Utils.checkChecksum(accountNumber) && Utils.checkChecksum(from)){
				if(amount>0){
					Account accountTo = accountDao.findByNumber(accountNumber);
					if(accountTo!=null){
						accountTo.setBalance(accountTo.getBalance()+amount);
						accountDao.save(accountTo);
						int balanceTo = accountTo.getBalance();
						
						ExternalTransfer transfer = new ExternalTransfer(title, amount, from, accountTo.getNumber(), balanceTo);
						externalTransferDao.save(transfer);

						return new ResponseEntity<>(headers, HttpStatus.CREATED);
					}else{	
						return new ResponseEntity<>(new ErrorResponse("Account does not exist!"), headers, HttpStatus.NOT_FOUND);	//404 konto nie istnieje
					}
				}else{
					return new ResponseEntity<>(new ErrorResponse("Negative ammount error!") ,headers, HttpStatus.BAD_REQUEST);	//400 ujemna kwota
				}
			}else{
				return new ResponseEntity<>(new ErrorResponse("Checksum error!"), headers, HttpStatus.FORBIDDEN);	//403 bład sumy kontrolnej
			}
		}catch(Exception e){
			return new ResponseEntity<>(new ErrorResponse("Server error!"), headers, HttpStatus.INTERNAL_SERVER_ERROR);	//500 błąd serwera
		}
	}
}
